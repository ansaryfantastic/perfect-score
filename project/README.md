# Taiko testing example

Example codebase to demonstrate taiko testing

## Installation

You need to install gauge and taiko globally.
Then run `npm install`.

## Testing

Run `npm test`
